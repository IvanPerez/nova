import {UserService} from '../../../../src/api/user/service/UserService';
import {User} from '../../../../src/api/user/model/User';
import bcrypt from 'bcrypt';
import {InvalidUserError} from '../../../../src/api/user/model/error/InvalidUserError';

describe('UserService', () => {
  test('Validation should allow a valid user', async () => {
    const password = 'ExamplePassword1234!';
    const user = new User();
    user.email = 'test@example.com';
    user.password = await bcrypt.hash(password, 10);
    const userService = new UserService(new UserRepositoryMock(user) as any);

    const result = await userService.validate('test', password);
    expect(result).toBeInstanceOf(User);
  });

  test('Validation should not allow an invalid user', async () => {
    const password = 'ExamplePassword1234!';
    const user = new User();
    user.email = 'test@example.com';
    user.password = await bcrypt.hash(password, 10);
    const userService = new UserService(new UserRepositoryMock(user) as any);

    const promise = userService.validate('test', 'other-password');
    await expect(promise).rejects.toThrow(InvalidUserError);
  });
});

class UserRepositoryMock {
  public constructor(private user: User) {
  }

  public findByEmail(email: string): Promise<User | undefined> {
    return Promise.resolve(this.user);
  }
}
