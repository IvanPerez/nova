import {NominationService} from '../../../../src/api/nomination/service/NominationService';
import {NominationInput} from '../../../../src/api/nomination/model/NominationInput';
import {User} from '../../../../src/api/user/model/User';
import {BadRequestError} from 'routing-controllers';
import {Email} from '../../../../src/api/email/model/Email';
import {Nomination, NominationStatus} from '../../../../src/api/nomination/model/Nomination';

describe('NominationService', () => {
  test('Nominate should validate input parameters', async () => {
    const nominationService = new NominationService(
      new NominationRepositoryMock() as any,
      new UserRepositoryMock() as any,
      new EmailServiceMock() as any,
    );
    const input = new NominationInput();
    const user = new User();

    const promise = expect(nominationService.nominate(input, user));
    await promise.rejects.toThrow(BadRequestError);
    await promise.rejects.toThrow('Email is undefined');
  });

  test('Nominate should reject if overall score is less than the threshold', async () => {
    const nominationService = new NominationService(
      new NominationRepositoryMock() as any,
      new UserRepositoryMock() as any,
      new EmailServiceMock() as any,
    );
    const input = new NominationInput();
    input.email = 'test@example.com';
    input.involvementOtherCommunitiesScore = 5;
    input.overallTalentScore = NominationService.MINIMUM_OVERALL_TALENT_SCORE * 0.9;
    const user = new User();

    const promise = nominationService.nominate(input, user);
    await expect(promise).resolves.toBeInstanceOf(Nomination);

    const result = await promise;
    await expect(result.status).toBe(NominationStatus.REJECTED);
  });
});

class NominationRepositoryMock {
  public save(): Promise<void> {
    return Promise.resolve();
  }
}

class UserRepositoryMock {
  public findByEmail(email: string): Promise<User | undefined> {
    return Promise.resolve(undefined);
  }
}

class EmailServiceMock {
  public send(email: Email): Promise<void> {
    return Promise.resolve();
  }
}
