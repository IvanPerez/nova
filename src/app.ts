import 'reflect-metadata';
import {logger} from './lib/Logger';
import {
  Action,
  createExpressServer,
  getMetadataArgsStorage,
  RoutingControllersOptions,
  UnauthorizedError,
  useContainer
} from 'routing-controllers';
import {Container, Service} from 'typedi';
import path from 'path';
import {createConnection} from 'typeorm';
import {Application} from 'express';
import {User} from './api/user/model/User';
import {Role} from './api/user/model/Role';
import auth from 'basic-auth';
import {UserService} from './api/user/service/UserService';
import {InvalidUserError} from './api/user/model/error/InvalidUserError';
import {Environment} from './lib/Environment';
import {routingControllersToSpec} from 'routing-controllers-openapi';
import swaggerUiExpress from 'swagger-ui-express';
import {validationMetadatasToSchemas} from 'class-validator-jsonschema';

@Service()
class App {
  public constructor(private userService: UserService,
                     private environment: Environment,
  ) {
  }

  public getPort(): number {
    return this.environment.getNumber('PORT');
  }

  public run(): Promise<void> {
    const options = {
      routePrefix: '/api/v1',
      controllers: [path.join(__dirname, '/api/*/controller/*.js')],
      development: !this.environment.getBoolean('PRODUCTION', true),
      authorizationChecker: async (action: Action, roles: string[]): Promise<boolean> => {
        const user = await this.getUser(action);
        if (user === undefined) {
          return false;
        }
        if (roles.includes(Role.ADMIN)) {
          if (!user.isAdmin) {
            return false;
          }
        }
        return true;
      },
      currentUserChecker: (action: Action): Promise<User> => {
        return this.getUser(action);
      },
    };
    const app: Application = createExpressServer(options);
    this.setupOpenApi(app, options);
    return new Promise<void>((resolve, reject) => {
      app.listen(this.getPort(), () => {
        resolve();
      }).on('error', error => {
        reject(error);
      });
    });
  }

  private setupOpenApi(app: Application, routingControllersOptions: RoutingControllersOptions): void {
    if (this.environment.getBoolean('PRODUCTION', true)) {
      return;
    }

    const schemas = validationMetadatasToSchemas({
      refPointerPrefix: '#/components/schemas/',
    });
    const spec = routingControllersToSpec(getMetadataArgsStorage(), routingControllersOptions, {
      components: {
        schemas,
        securitySchemes: {
          basicAuth: {
            scheme: 'basic',
            type: 'http',
          },
        },
      },
      info: {
        title: 'Nova Technical Challenge',
        version: '1.0',
      },
    });
    app.use('/api-docs', swaggerUiExpress.serve, swaggerUiExpress.setup(spec));
  }

  private async getUser(action: Action): Promise<User> {
    const authorization = auth(action.request);
    if (authorization === undefined) {
      throw new UnauthorizedError();
    }
    return this.userService.validate(authorization.name, authorization.pass).catch(error => {
      if (error instanceof InvalidUserError) {
        throw new UnauthorizedError();
      } else {
        throw error;
      }
    });
  }
}

useContainer(Container);
createConnection('default').then(() => {
  const app = Container.get(App);
  app.run().then(() => {
    logger.info('Server running at port ' + app.getPort());
  });
});

