import {Service} from 'typedi';
import {User} from '../model/User';
import {getRepository, Repository} from 'typeorm';

@Service()
export class UserRepository {
  private readonly repository: Repository<User>;

  public constructor() {
    this.repository = getRepository(User);
  }

  public find(id: number): Promise<User | undefined> {
    return this.repository.findOne(id);
  }

  public findByEmail(email: string): Promise<User | undefined> {
    return this.repository.findOne({
      email,
    });
  }
}
