import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  public id: number | null = null;

  @Column({ unique: true })
  public email: string = '';

  @Column()
  public password: string = '';

  @Column()
  public isAdmin: boolean = false;
}
