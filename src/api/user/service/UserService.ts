import {Service} from 'typedi';
import {UserRepository} from '../repository/UserRepository';
import {User} from '../model/User';
import bcrypt from 'bcrypt';
import {InvalidUserError} from '../model/error/InvalidUserError';

@Service()
export class UserService {
  public constructor(private userRepository: UserRepository) {
  }

  public async validate(email: string, password: string): Promise<User> {
    const user = await this.userRepository.findByEmail(email);
    if (user === undefined) {
      throw new InvalidUserError()
    }
    if (! await bcrypt.compare(password, user.password)) {
      throw new InvalidUserError();
    }
    return user;
  }
}
