import {Service} from 'typedi';
import nodemailer, {Transporter} from 'nodemailer';
import {Email} from '../model/Email';
import {Environment} from '../../../lib/Environment';

@Service()
export class EmailService {
  private readonly transporter: Transporter;
  private readonly from: string;

  public constructor(environment: Environment) {
    this.transporter = nodemailer.createTransport({
      host: environment.getString('MAIL_SMTP_HOST'),
      auth: {
        user: environment.getString('MAIL_SMTP_USER'),
        pass: environment.getString('MAIL_SMTP_PASSWORD'),
      },
      secure: true,
      port: environment.getNumber('MAIL_SMTP_PORT'),
    });
    this.from = environment.getString('MAIL_SMTP_FROM');
  }

  public send(email: Email): Promise<void> {
    return this.transporter.sendMail({
      from: this.from,
      to: email.to,
      subject: email.subject,
      text: email.text,
    });
  }
}
