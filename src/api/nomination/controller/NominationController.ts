import {Authorized, Body, CurrentUser, Get, HttpCode, JsonController, Post} from 'routing-controllers';
import {Nomination} from '../model/Nomination';
import {NominationService} from '../service/NominationService';
import {Inject, Service} from 'typedi';
import {User} from '../../user/model/User';
import {NominationAdminOutput} from '../model/NominationAdminOutput';
import {NominationInput} from '../model/NominationInput';
import {Role} from '../../user/model/Role';
import {StatusCodes} from 'http-status-codes';
import {OpenAPI} from 'routing-controllers-openapi';

@JsonController('/nominations')
@Service()
@OpenAPI({
  summary: 'Nominations manager',
})
export class NominationController {
  public constructor(@Inject() private nominationService: NominationService) {
  }

  @Post()
  @Authorized()
  @HttpCode(StatusCodes.CREATED)
  @OpenAPI({ description: 'Nominate a user' })
  public nominate(@Body() nominationInput: NominationInput,
                  @CurrentUser({ required: true }) currentUser: User,
  ): Promise<Nomination> {
    return this.nominationService.nominate(nominationInput, currentUser);
  }

  @Get()
  @Authorized(Role.ADMIN)
  @OpenAPI({
    description: 'Get all non-rejected nominations',
    responses: {
      403: {
        description: 'If the user doesn’t have enough permissions',
      },
    },
  })
  public getNonRejected(): Promise<NominationAdminOutput[]> {
    return this.nominationService.getNonRejected()
      .then(list => list.map(nomination => new NominationAdminOutput(nomination)));
  }
}
