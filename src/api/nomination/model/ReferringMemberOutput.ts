import {User} from '../../user/model/User';

export class ReferringMemberOutput {
  private readonly id: number;
  private readonly email: string;

  public constructor(user: User) {
    this.id = user.id ?? NaN;
    this.email = user.email;
  }
}
