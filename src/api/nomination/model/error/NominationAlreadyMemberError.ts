import {NotFoundError} from 'routing-controllers';

export class NominationAlreadyMemberError extends NotFoundError {
  public constructor() {
    super('This user already exists');
  }
}
