import {BadRequestError} from 'routing-controllers';

export class InvalidNominationError extends BadRequestError {
  public constructor(message: string) {
    super(message);
  }
}
