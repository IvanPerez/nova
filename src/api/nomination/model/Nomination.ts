import {User} from '../../user/model/User';
import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';

export enum NominationStatus {
  PENDING = 'pending',
  ACCEPTED = 'accepted',
  REJECTED = 'rejected',
}

@Entity()
export class Nomination {
  @PrimaryGeneratedColumn()
  public id: number | null = null;

  @ManyToOne(type => User, { nullable: false })
  public referringMember: User = new User();

  @Column()
  public email: string = '';

  @Column()
  public description: string = '';

  @Column({ type: 'float' })
  public involvementOtherCommunitiesScore: number = -1;

  @Column({ type: 'float' })
  public overallTalentScore: number = -1;

  @Column({ enum: NominationStatus })
  public status: NominationStatus = NominationStatus.PENDING;
}
