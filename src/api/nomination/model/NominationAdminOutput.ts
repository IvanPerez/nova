import {Nomination} from './Nomination';
import {ReferringMemberOutput} from './ReferringMemberOutput';

export class NominationAdminOutput {
  private readonly email: string;
  private readonly description: string;
  private readonly involvementOtherCommunitiesScore: number;
  private readonly overallTalentScore: number;
  private readonly referringMember: ReferringMemberOutput;

  public constructor(nomination: Nomination) {
    this.email = nomination.email;
    this.description = nomination.description;
    this.involvementOtherCommunitiesScore = nomination.involvementOtherCommunitiesScore;
    this.overallTalentScore = nomination.overallTalentScore;
    this.referringMember = new ReferringMemberOutput(nomination.referringMember);
  }
}
