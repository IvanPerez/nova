import {Nomination} from './Nomination';
import {User} from '../../user/model/User';
import {InvalidNominationError} from './error/InvalidNominationError';

export class NominationInput {
  public email: string | undefined;
  public description: string = '';
  public involvementOtherCommunitiesScore: number | undefined;
  public overallTalentScore: number | undefined;

  public toNomination(member: User): Nomination {
    if (this.email === undefined) {
      throw new InvalidNominationError('Email is undefined');
    }
    if (this.involvementOtherCommunitiesScore === undefined) {
      throw new InvalidNominationError('InvolvementOtherCommunitiesScore is undefined');
    }
    if (this.involvementOtherCommunitiesScore < 0 || this.involvementOtherCommunitiesScore > 10) {
      throw new InvalidNominationError('InvolvementOtherCommunitiesScore must be a value between 0 and 10')
    }
    if (this.overallTalentScore === undefined) {
      throw new InvalidNominationError('OverallTalentScore is undefined');
    }
    if (this.overallTalentScore < 0 || this.overallTalentScore > 10) {
      throw new InvalidNominationError('OverallTalentScore must be a value between 0 and 10')
    }

    const nomination = new Nomination();
    nomination.email = this.email;
    nomination.description = this.description;
    nomination.involvementOtherCommunitiesScore = this.involvementOtherCommunitiesScore;
    nomination.overallTalentScore = this.overallTalentScore;
    nomination.referringMember = member;
    return nomination;
  }
}
