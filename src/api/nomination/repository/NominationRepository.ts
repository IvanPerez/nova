import {Service} from 'typedi';
import {Nomination, NominationStatus} from '../model/Nomination';
import {getRepository, Not, Repository} from 'typeorm';

@Service()
export class NominationRepository {
  private readonly repository: Repository<Nomination>;

  public constructor() {
    this.repository = getRepository(Nomination);
  }

  public getNonRejected(): Promise<Nomination[]> {
    return this.repository.find({
      where: {
        status: Not(NominationStatus.REJECTED),
      },
      relations: ['referringMember'],
    });
  }

  public save(nomination: Nomination): Promise<Nomination> {
    return this.repository.save(nomination);
  }
}
