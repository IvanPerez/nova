import {Service} from 'typedi';
import {Nomination, NominationStatus} from '../model/Nomination';
import {EmailService} from '../../email/service/EmailService';
import {User} from '../../user/model/User';
import {NominationRepository} from '../repository/NominationRepository';
import {NominationInput} from '../model/NominationInput';
import {UserRepository} from '../../user/repository/UserRepository';
import {Email} from '../../email/model/Email';
import {NominationAlreadyMemberError} from '../model/error/NominationAlreadyMemberError';

@Service()
export class NominationService {
  public static readonly MINIMUM_OVERALL_TALENT_SCORE = 8;

  public constructor(private nominationRepository: NominationRepository,
                     private userRepository: UserRepository,
                     private emailService: EmailService,
  ) {
  }

  public async nominate(nominationInput: NominationInput, user: User): Promise<Nomination> {
    const nomination = nominationInput.toNomination(user);
    if (await this.userRepository.findByEmail(nomination.email) !== undefined) {
      throw new NominationAlreadyMemberError();
    }
    if (nomination.overallTalentScore < NominationService.MINIMUM_OVERALL_TALENT_SCORE) {
      nomination.status = NominationStatus.REJECTED;
    }
    await this.nominationRepository.save(nomination);

    if (nomination.status === NominationStatus.REJECTED) { // send email to candidate and to the referring member (async - not waiting for completion)
      this.emailService.send(new Email(nomination.email, '[NOVA] Your candidature was rejected', '...'));
      this.emailService.send(new Email(user.email, '[NOVA] Rejected nomination', `Your nomination of ${nomination.email} was rejected.`));
    }

    return nomination;
  }

  public getNonRejected(): Promise<Nomination[]> {
    return this.nominationRepository.getNonRejected();
  }
}
