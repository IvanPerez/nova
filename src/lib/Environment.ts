import 'dotenv/config';

import {Service} from 'typedi';

@Service()
export class Environment {
  public getString(name: string, defaultValue?: string): string {
    const value = process.env[name] ?? defaultValue;
    if (value === undefined) {
      throw new Error('Environment variable not found: ' + name);
    }
    return value;
  }

  public getNumber(name: string, defaultValue?: number): number {
    const valueString = this.getString(name);
    const value = valueString !== undefined ? parseFloat(valueString) : defaultValue;
    if (value === undefined || isNaN(value)) {
      throw new Error('Environment variable is not a valid number: ' + name);
    }
    return value;
  }

  public getBoolean(name: string, defaultValue?: boolean): boolean {
    const value = this.getString(name);
    if (value === 'true') {
      return true;
    } else if (value === 'false') {
      return false;
    } else {
      if (defaultValue !== undefined) {
        return defaultValue;
      } else {
        throw new Error('Environment variable is not a valid boolean: ' + name);
      }
    }
  }
}
