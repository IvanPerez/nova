# Nova Technical Challenge

## Run

You need to install the dependencies first (`npm install`).

Build (it will put the compilation results in the `dist` folder): `npm run build`

Before you run the application for the first time, you need to set some configuration in the `.env` file.
You have a `.env.sample` that you can use as a sample to create the environment config.

Finally, you can run the application: `npm start` 

### Notes

This API will use a SQLite database, which will be created automatically on the first run on the `data` directory.

Available endpoints:
* **GET http://localhost:8080/api/v1/nominations** (list all non-rejected nominations - for admins only).
* **POST http://localhost:8080/api/v1/nominations** (create a nomination - for authenticated users only).

These endpoints are documented in **Swagger**: http://localhost:8080/api-docs/ (avaiblable if the application is not run in production mode).

User authentication is made using HTTP Basic Authorization.

## Instructions

Nova is the global top-talent community which connects high potential individuals amongst themselves and with the best opportunities.

Imagine you have been hired by Nova and this is your first task. You have a few days to complete this assignment.

Implement and provide the solution of a simple API to support the following hypothetical Nomination use cases:

* Nova members can nominate their peers by providing their email, a short explanation on why the person is a good fit, and a score between 0 and 10 of: (1) the candidate’s involvement with other communities, and (2) overall talent.
  * Nominations of emails that are already in the network are not accepted nor stored by the system. All the rest of the nominations of candidates are stored.
  * Stored nominations that have less than 8 in overall talent are automatically rejected and trigger and email to both the referrer and the candidate explaining the rejection.
* An admin can list all the non-rejected nominations, including who is the referring Nova member.

Keep in mind the following details:
* The test can be completed using Javascript or Typescript
* Use any type of storage you want (MongoDB, Postgres, SQLite, files, etc.)
* Even though it’s a simple API make sure you organize it good enough for scalability and maintainability
* Make something that makes you proud!

That’s all! Don’t hesitate to email us if you have any doubt and good luck!
